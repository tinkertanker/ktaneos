#define KTANE_I_NEED_SOLVING  1 // 1 if this module needs to be solved, 0 otherwise

long valueLastSentTime;

void setup() {
  ktaneSetup(); // setup ktane

  // put your own setup code here, to run once:
  valueLastSentTime = millis();
  pinMode(A0, INPUT);
  pinMode(13, OUTPUT);
  digitalWrite(13, LOW);
}

void loop() {
  ktaneLoop(); // loop ktane regularly to let it do its thing

  // put your own main code here, to run repeatedly:
  if (millis() - valueLastSentTime > 3000) {
    ktaneSendMessage("reading", analogRead(A0));
    valueLastSentTime = millis();
  }
  delay(100);
}

void ktaneMessageReceived(String subject, int message) {
  // insert your code for message handling if you want
}

/*
 * ktaneOS Core Stuff
 * 
 */

void ktaneSetup() {
  Serial.begin(115200);
}

void ktaneSendMessage(String subject, int message) {
  Serial.print("#");
  Serial.print(subject);
  Serial.print(":");
  Serial.print(message);
  Serial.print("\n");
}

void ktaneLoop() {
  while (Serial.available()) {
    digitalWrite(13, HIGH);
    char incomingChar = Serial.read();
    if (incomingChar == '#') {
      // start of sequence char detected
      String subject = Serial.readStringUntil(':');
      int message = Serial.parseInt();
      if (subject == "count") {
        // send on the count, increasing if I need to be solved
        ktaneSendMessage(subject, message + KTANE_I_NEED_SOLVING);
        // call handler for receiving messages
        ktaneMessageReceived(subject, message);
      } else {
        // propagate same message on
        ktaneSendMessage(subject, message);
        // call handler for receiving messages
        ktaneMessageReceived(subject, message);
      }
    } // else ignore noise
    digitalWrite(13, LOW);
  }
}

